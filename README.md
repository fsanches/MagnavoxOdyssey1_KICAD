I'm releasing my transcription (the KICAD files in this repository) of the original Magnavox Odyssey 1 home video-game console to the public domain.
-- Felipe Correa da Silva Sanches <juca@members.fsf.org> May 14th, 2017

The original drawings from service manual were scanned by David WINTER.
